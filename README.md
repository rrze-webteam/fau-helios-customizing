Customizing für vote.fau.de


# Git

Repo: https://gitlab.rrze.fau.de/rrze-webteam/fau-helios-customizing

Die hier produzierten Templates dienen nur dem Customizing.
Die eigentlichen Sourcen des Helios-Voting Systems befinden sich im Git-Projekt
https://gitlab.rrze.fau.de/heliosvoting/helios-server



## Dev info

  REPLACE 
  /static/     width 
  ../helios-server/server_ui/media/



zu ändernde Parameter und Settings:

    {{ settings.MAIN_LOGO_URL }}
    
    liegt nun in 
	/helios-server/server_ui/media/fau/img/fauhelios.png


    {{settings.WELCOME_MESSAGE|safe}}
	ändern auf:

	Online Abstimmungen und Wahlen an der Friedrich-Alexander-Universität Erlangen-Nürnberg (FAU).


    {{login_box|safe}}	

    Ausgaben so ändern, dass der Link wie folgt in HTML formatiert ist_
	
	    <p>
	        <a href="{{settings.SECURE_URL_HOST}}{% url "auth@index" %}?return_url={{CURRENT_URL}}" class="button"><span>Anmelden</span> <span class="langtext" lang="en">Login</span></a>
	    </p>


    Links:
	/accessibility  
	Link ins server_ui/urls.py  ergänzen, sowie an weiteren notwendigen Stellen um die neue Seite aufzurufen

	Sowie in views.py

	    def accessibility(request):
	     return render_template(request, "accessibility")
