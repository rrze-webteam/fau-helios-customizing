'use strict';

const
    {src, dest, watch, series} = require('gulp'),
    sass = require('gulp-sass'),
    cleancss = require('gulp-clean-css'),
    postcss = require('gulp-postcss'),
    autoprefixer = require('autoprefixer'),
    bump = require('gulp-bump'),
    semver = require('semver'),
    info = require('./package.json'),
    rename = require('gulp-rename'),
    touch = require('gulp-touch-cmd')
;

function css() {
    return src('./src/sass/fau-helios.scss', {
            sourcemaps: false
        })
        .pipe(sass())
        .pipe(postcss([autoprefixer()]))
        .pipe(cleancss())
        .pipe(dest(info.mediapath + './css'))
	.pipe(touch());
}
function cssdev() {
    return src('./src/sass/fau-helios.scss', {
            sourcemaps: true
        })
        .pipe(sass())
        .pipe(postcss([autoprefixer()]))
        .pipe(dest(info.mediapath +'./css'))
	.pipe(touch());
}

function patchPackageVersion() {
    var newVer = semver.inc(info.version, 'patch');
    return src(['./package.json'])
        .pipe(bump({
            version: newVer
        }))
        .pipe(dest('./'))
	.pipe(touch());
};
function prereleasePackageVersion() {
    var newVer = semver.inc(info.version, 'prerelease');
    return src(['./package.json'])
        .pipe(bump({
            version: newVer
        }))
	.pipe(dest('./'))
	.pipe(touch());;
};

function logo() {
    return src(['./src/img/' + info.logofile])
	.pipe(rename('fauhelios.png'))
	.pipe(dest(info.mediapath +'./img'))
        .pipe(touch());
}
function loginbackground() {
    return src(['./src/img/background/' + info.loginbackground])
	.pipe(rename('login-background.jpg'))
	.pipe(dest(info.mediapath +'./img'))
        .pipe(touch());
}
function js() {
    return src('./src/js/*.js')
        .pipe(dest(info.mediapath +'./js'))
	.pipe(touch());
}



exports.css = css;
exports.dev = series(cssdev, prereleasePackageVersion, js, logo, loginbackground);
exports.build = series(css, patchPackageVersion, js, logo, loginbackground);

